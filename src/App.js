import React, { Component } from 'react';
import CurriculumAPI from './api';
import Header from './Header';
import Main from './Main'

class App extends Component {
  state = {
    header: {}
  }

  componentDidMount() {
    /**
     * get header info and save to state
     */
    CurriculumAPI.getHeader()
      .then(header => {
        this.setState({ header });
      })
  }

  render() {
    return (
      <div className="container">
        <Header header={this.state.header} />
        <div className="row">
          <div className="col s12">
            <Main />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
