import React, { Component } from "react";
import EntryItem from './EntryItem'

class EntryList extends Component {
  render() {
    var section = this.props.section;
    var listItems = section.children.map((entry, index) =>
      <EntryItem entry={entry} key={index} />
    );

    return (
      <div className="card">
        <div className="card-content">
          <span className="card-title">{section.title}</span>
          <div className="card-content">
            {listItems}
          </div>
        </div>
      </div>
    );
  }
}

export default EntryList;
