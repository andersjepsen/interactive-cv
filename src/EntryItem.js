import React, { Component } from "react";
import { Link } from 'react-router-dom'

class EntryItem extends Component {
  render() {
    var entry = this.props.entry;

    var avatar = entry.avatar ? 
      <div className="col">
        <img src={entry.avatar} alt={entry.location} />
      </div> : undefined;

    var dateRange = entry.start_date ? <p>{entry.start_date} - {entry.end_date || 'present'}</p> : undefined;

    return (
      <Link to={`${entry.path}`}>
        <div className="row">
          {avatar}
          <div className="col">
            <strong>{entry.title}</strong>
            <p>{entry.location}</p>
            {dateRange}
          </div>
        </div>
      </Link>
    );
  }
};

export default EntryItem;