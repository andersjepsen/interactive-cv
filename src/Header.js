import React, { Component } from "react";
import { Link } from 'react-router-dom'
import './Header.css';

class Header extends Component {
  render() {
    var header = this.props.header || {};

    return (
      <header>
        <div className="row">
          <div className="col">
          <Link to="/">
            <img className="avatar" src={header.avatar} alt={header.first_name + ' ' + header.last_name} />
          </Link>
          </div>
          <div className="col">
            <h2>{header.first_name} {header.last_name}</h2>
            <h4>Curriculum Vitae</h4>
          </div>
        </div>
      </header>
    );
  }
};

export default Header;