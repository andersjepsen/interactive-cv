import axios from 'axios'

const CurriculumAPI = {
  /**
   * Get header information
   * @returns {Object}
   */
  async getHeader() {
    return axios.get('/data/cv.json')
      .then(response => {
        return response.data.header;
      });
  },

  /**
   * Get a list of all sections
   * @returns {Array}
   */
  async getSections() {
    return axios.get('/data/cv.json')
    .then(response => {
      return response.data.sections;
    });
  },

  /**
   * Get a specific entry detail
   * @param {*} detail - the type of detail (experience, skill, etc)
   * @param {*} id - entry id
   * @returns {Object}
   */
  async getDetail(detail, id) {
    return axios.get('/data/cv.json')
    .then(response => {
      var section = response.data.sections.find(s => s.path === detail);
      return section.children.find(d => d.id === Number(id))
    });
  }
}

export default CurriculumAPI