import React, { Component } from "react";
import CurriculumAPI from './api'

class EntryDetail extends Component {
  state = {
    entry: undefined
  }

  componentDidMount() {
    /**
     * get id and detail from url
     */
    var id = this.props.match.params.id;
    var detail = this.props.match.params.detail;

    /**
     * get detail info and save to state
     */
    CurriculumAPI.getDetail(detail, id)
    .then(entry => {
      this.setState({ entry });
    })
  }
  
  render() {    
    var entry = this.state.entry;

    if (!entry) {
      return <div>Ingen detalje fundet</div>
    }

    var cardImage = entry.avatar_large ? 
      <div className="card-image">
        <img src={entry.avatar_large} alt={entry.title}/>
      </div> : undefined;

    var dateRange = entry.start_date ? <p>{entry.start_date} - {entry.end_date || 'present'}</p> : undefined;

    return (
      <div className="card">
        {cardImage}
        <div className="card-content">
          <span className="card-title">{entry.title}</span>
          <div className="row">
            <div className="col">
              <p>{entry.location}</p>
              {dateRange}
              <p>{entry.description}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default EntryDetail;