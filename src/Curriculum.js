import React, { Component } from 'react';
import CurriculumAPI from './api';
import EntryList from "./EntryList";

class Curriculum extends Component {
  state = {
    sections: []
  }

  componentDidMount() {
    /**
     * Get sections and save to state
     */
    CurriculumAPI.getSections()
      .then(sections => {
        this.setState({ sections });
      })
  }

  render() {
    var entryLists = this.state.sections.map((section, index) =>
      <EntryList section={section} key={index} />
    );
    return (
      <div>
        {entryLists}
      </div>
    );
  }
}

export default Curriculum;
