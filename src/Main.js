import React, { Component } from "react";
import { Switch, Route } from 'react-router-dom'
import Curriculum from './Curriculum'
import EntryDetail from './EntryDetail'

class Main extends Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={Curriculum} />
          <Route path='/:detail/:id' component={EntryDetail} />
        </Switch>
      </main>
    );
  }
}

export default Main;
